import {{ cookiecutter.module_name }}


def test_cli_example_subcommand(set_cli_args):
    """Calling the example subcommand with no options succeeds."""
    set_cli_args("{{ cookiecutter.package_name }}", "example-subcommand")
    {{ cookiecutter.module_name }}.main()
