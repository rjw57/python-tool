import importlib.metadata

import {{ cookiecutter.module_name }}


def test_cli_version(set_cli_args, capsys):
    """Passing --version on the command line prints out the package version."""
    set_cli_args("{{ cookiecutter.package_name }}", "--version")
    expected_version = importlib.metadata.version("{{ cookiecutter.module_name }}")
    {{ cookiecutter.module_name }}.main()
    assert capsys.readouterr().out.strip() == expected_version
