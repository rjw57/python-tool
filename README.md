# Python command line tool boilerplate

This repository contains a
[cookiecutter](https://github.com/audreyr/cookiecutter) template for Python
command line tools.

## Quickstart

```bash
pip3 install cookiecutter
cookiecutter --no-input https://gitlab.developers.cam.ac.uk/uis/devops/cookies/python-tool.git \
    project_name="{YOUR PROJECT NAME HERE}"
```

This will create a directory named after your project. You should `cd` into
that directory and run `git init` to create a git repo from it.

## Testing

The repository is tested via the `tox` test runner. The tests include a basic
check that the template can generate output and that running tox and pre-commit
checks in the generated output succeeds.
