import sys

import pytest


@pytest.fixture
def set_cli_args(monkeypatch):
    """A function which sets command-line arguments returned by sys.argv."""
    return lambda *args: monkeypatch.setattr(sys, "argv", args)
